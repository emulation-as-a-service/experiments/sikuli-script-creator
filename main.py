#!/usr/bin/env python3
import base64
import json

# TODO potentially update python Versions at eaas backend to use case matching/walrus operator
import os
import shutil
import sys

DEFAULT_TIMEOUT_VALUE = 30
SIKULI_OUTPUT = ""
IS_DEBUG = False  # TODO Flag!


def main():
    global SIKULI_OUTPUT
    global IS_DEBUG

    print("Starting Sikuli Script Creation...")

    if len(sys.argv) > 3:
        if "debug" in sys.argv[3]:
            IS_DEBUG = True

    if not IS_DEBUG:
        SIKULI_OUTPUT = sys.argv[2] + "/output"

    else:
        SIKULI_OUTPUT = "output"

    # if os.path.exists(SIKULI_OUTPUT):
    #     shutil.rmtree(SIKULI_OUTPUT)
    os.mkdir(SIKULI_OUTPUT)

    with open(sys.argv[1]) as json_file:

        print("Opening:", sys.argv[1])

        data = json.load(json_file)

        images = data["images"]

        for k, v in images.items():
            base64_to_image(v["img"], k)

        elements = data["elements"]

        if not any(x for x in elements if
                   x["t"] == "unknown" and "setAutoWaitTimeout" in x["content"]):
            write_to_script(f"SCREEN.setAutoWaitTimeout({DEFAULT_TIMEOUT_VALUE})")

        for script_entry in elements:

            t = script_entry["t"]

            # if (t := script_entry["t"]) == "click":
            if t == "click":
                img_name = f"img_{script_entry['imgId']}.png"

                o_x, o_y = get_offset(script_entry)
                btn = script_entry["button"]

                if btn == "left":
                    write_to_script(f"click(Pattern(\"{img_name}\").targetOffset({o_x},{o_y}))")
                if btn == "right":
                    write_to_script(
                        f"rightClick(Pattern(\"{img_name}\").targetOffset({o_x},{o_y}))")
                if btn == "double":
                    write_to_script(
                        f"doubleClick(Pattern(\"{img_name}\").targetOffset({o_x},{o_y}))")

            elif t == "wait":
                img_name = f"img_{script_entry['imgId']}.png"
                o_x, o_y = get_offset(script_entry)

                write_to_script(
                    f"wait(Pattern(\"{img_name}\").targetOffset({o_x},{o_y}))")

            elif t == "hover":
                img_name = f"img_{script_entry['imgId']}.png"
                o_x, o_y = get_offset(script_entry)

                write_to_script(f"hover(Pattern(\"{img_name}\").targetOffset({o_x},{o_y}))")

            elif t == "sleep":
                write_to_script(f"wait({script_entry['time']})")

            elif t == "type":
                write_to_script(f"type(\"{script_entry['text']}\")")

            elif t == "unknown":
                write_to_script(script_entry['content'])

            else:
                print(f"ERROR, unknown type {t}, not writing ...")


def get_offset(script_entry):
    if script_entry.get("offset", None):

        o_x = script_entry["offset"]["x"]
        o_y = script_entry["offset"]["y"]
    else:
        o_x = o_y = 0
    return o_x, o_y


def base64_to_image(base64_string, nr):
    b64img = str(base64_string)[22:]

    # print(b64img)

    with open(f"{SIKULI_OUTPUT}/img_{nr}.png", "wb+") as fh:
        fh.write(base64.decodebytes(b64img.encode()))

    print("Stored image @", f"{SIKULI_OUTPUT}/img_{nr}.png")

    return f"img_{nr}.png"


def write_to_script(content):
    print("Writing to script:", content)
    with open(f"{SIKULI_OUTPUT}/script.py", "a") as script_file:
        script_file.write(content)
        script_file.write("\n")


if __name__ == '__main__':
    main()
