#!/usr/bin/env python3
import base64
import json
import os.path
import re
import sys

rev_dict = {}
elements = []
images = {}


# TODO support - in images instead of \w only
def main():
    arg = sys.argv[1]

    with open(arg) as script:

        global rev_dict
        global elements
        global images

        print("Starting reverse script generation...")

        abspath = os.path.abspath(arg)
        directory, _ = os.path.split(abspath)
        print("Using directory:", directory)
        print("Opening:", arg)

        for i, line in enumerate(script.read().splitlines()):

            entry = {}
            # TODO try catch!!! if error continue anyway...

            if not line:
                continue

            try:
                if line.startswith("click"):
                    m = re.findall(r"[\"'][\w-]+\.png[\"']", line)
                    click_match = m[0][1:-1]
                    img_id = get_image_id(click_match, directory)
                    entry = {"t": "click", "button": "left", "imgId": img_id, "index": i}

                elif line.startswith("doubleClick"):
                    m = re.findall(r"[\"'][\w-]+\.png[\"']", line)
                    click_match = m[0][1:-1]
                    img_id = get_image_id(click_match, directory)
                    entry = {"t": "click", "button": "double", "imgId": img_id, "index": i}

                elif line.startswith("rightClick"):
                    m = re.findall(r"[\"'][\w-]+\.png[\"']", line)
                    click_match = m[0][1:-1]
                    img_id = get_image_id(click_match, directory)
                    entry = {"t": "click", "button": "right", "imgId": img_id, "index": i}

                elif line.startswith("wait"):

                    m = re.findall(r"[\"'][\w\.-]+[\"']", line)
                    if m:
                        wait_match = m[0][1:-1]
                        img_id = get_image_id(wait_match, directory)
                        entry = {"t": "wait", "imgId": img_id, "index": i}

                    else:
                        m = re.findall(r"\d+", line)
                        t_time = m[0]
                        entry = {"t": "sleep", "time": t_time, "index": i}

                elif line.startswith("type"):
                    m = re.findall(r"[\"'][\w\s]+[\"']", line)  # TODO add type(Key.ENTER) etc.
                    t_text = m[0][1:-1]
                    entry = {"t": "type", "text": t_text, "index": i}

                elif line.startswith("hover"):
                    m = re.findall(r"[\"'][\w-]+\.png[\"']", line)
                    hover_match = m[0][1:-1]
                    img_id = get_image_id(hover_match, directory)
                    entry = {"t": "hover", "imgId": img_id, "index": i}

                else:
                    entry = {"t": "unknown", "content": line}

                offset_match = re.findall(r"targetOffset\([-\d]+,[-\d]+\)", line)
                if offset_match:
                    # print(offset_match)
                    # print("found offset:", offset_match[0][13:-1])
                    offset = list(map(int, offset_match[0][13:-1].split(",")))
                    entry["offset"] = {"x": offset[0], "y": offset[1]}



            except Exception as e:
                print("Error while reading input, ", line, "\nError", e)
                entry = {"t": "unknown", "content": line}

            print(f"Writing ({i}):", entry)
            elements.append(entry)

        # print(elements)
        # print("-------------")
        # print(images)

        images_for_json = {v["imgId"]: {"img": v["img"]} for v in images.values()}

        with open("reverse_script.json", "w+") as json_file:
            json.dump({"elements": elements, "images": images_for_json}, json_file)


def get_image_id(image_name, directory):
    global images

    if image_name in images.keys():
        return images[image_name]["imgId"]

    else:
        with open(directory + "/" + image_name, "rb") as img:
            encoded_string = base64.b64encode(img.read()).decode()

            data = {"imgId": str(len(images.keys()) + 1),
                    "img": f"data:image/png;base64,{encoded_string}"}

            images[image_name] = data

            return data["imgId"]


if __name__ == '__main__':
    main()
